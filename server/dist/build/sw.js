var __wpo = {
  "assets": {
    "main": [
      "/ced611daf7709cc778da928fec876475.eot",
      "/b7c9e1e479de3b53f1e4e30ebac2403a.woff",
      "/748ce0709962c59bcf5beefc046b50e8.svg",
      "/favicon.ico",
      "/79281281dbbe03395e8cee5fd218abdf.png",
      "/d41f55a78e6f49a5512878df1737e58a.ttf",
      "/e79bfd88537def476913f3ed52f4f4b3.eot",
      "/0cf70eea9b00fe0c198cece7efa250f0.jpg",
      "/37a0b1b4182c6d7a4393aa6f2db203ce.png",
      "/10e22ff7425634179e2bd73120edd5fc.png",
      "/570eb83859dc23dd0eec423a49e147fe.woff2",
      "/b01d0a62ec3aca1ee2e08573243b1901.png",
      "/ae88db1c637533b5930cab1330fda7be.png",
      "/012cf6a10129e2275d79d6adac7f3b02.woff",
      "/d56c1f5aa44dffac58ca036110f7fd74.png",
      "/b03fa95f72a5e59a26501b47f217a81a.png",
      "/a37b0c01c0baf1888ca812cc0508f6e2.ttf",
      "/965208574dd7682941b01182a37fecbd.png",
      "/a1f06b2c29dd94a18894849642a1ff48.png",
      "/18bc17990313373e021bc00a1ccbd2db.jpg",
      "/runtime~main.de03749dd05db6888013.js",
      "/"
    ],
    "additional": [
      "/vendor.5f3f8e5c86cd2ed11594.chunk.js",
      "/1.e0fec2262e76af1349eb.chunk.js",
      "/2.dcac4c350b44db2dfc9c.chunk.js",
      "/3.dcf741daca837765df13.chunk.js",
      "/4.0c5cea92add1b543b226.chunk.js",
      "/main.16b18bf45ae123060fc5.chunk.js",
      "/7.2e0c0be9ef7f4919726a.chunk.js",
      "/8.af98932aaa1c28b61110.chunk.js",
      "/9.14c101b26865ccde6156.chunk.js",
      "/10.cdce1c22bcc19009effe.chunk.js",
      "/11.c0dc4af915b38f8b0a97.chunk.js",
      "/12.c0bb5e23360892a2df98.chunk.js",
      "/13.7cffcce93b9bced53d05.chunk.js",
      "/14.986f39ef6cdbea251e19.chunk.js",
      "/15.b85af442f4b72f5a1ee9.chunk.js",
      "/16.e5213846c941c2ee0e19.chunk.js",
      "/17.0263b7abe7ad0ef75ffa.chunk.js",
      "/18.b60d665e4058ff49ac91.chunk.js",
      "/19.c3c4c95d2ab80b8ac642.chunk.js",
      "/20.324a83cb7b1e0037da23.chunk.js",
      "/21.d3a8f17999cd9fcfade1.chunk.js",
      "/22.9891379ebcb965747143.chunk.js",
      "/23.4e067b74f93ad91da545.chunk.js",
      "/24.e4e59087b3b62cb37e02.chunk.js",
      "/25.fb1d8fb98142a6e5d128.chunk.js",
      "/26.41e650a10520fa74b4ce.chunk.js",
      "/27.8e163394f68ba2fbf245.chunk.js",
      "/28.fb5233331e6210219469.chunk.js",
      "/29.cb2c83502a5fdd155d21.chunk.js",
      "/30.e96e5d83a29b399e9e65.chunk.js",
      "/31.f57dbaa62fc06630a355.chunk.js",
      "/32.9077c5c8822fed31ab9e.chunk.js",
      "/33.42ddf9319fb79ae2f9e2.chunk.js",
      "/34.6ecae660195d6e68ac68.chunk.js",
      "/35.4e09785ae9af1e5ddfbc.chunk.js",
      "/36.267aa19469af75e5de9e.chunk.js",
      "/37.04d517a04de8c240e326.chunk.js",
      "/38.fb649a802484fd9deab9.chunk.js",
      "/39.1801bdd15594e2300ed2.chunk.js",
      "/40.d233b1df05ba58716558.chunk.js",
      "/41.71cced34a66f39211c2c.chunk.js",
      "/42.b5ac23e7de7ff9da849d.chunk.js",
      "/43.aa5b678da96a9b7f3048.chunk.js"
    ],
    "optional": []
  },
  "externals": [
    "/",
    "https://fonts.googleapis.com/css?family=Open+Sans:400,600,700",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
  ],
  "hashesMap": {
    "2dff0768f4c0a53228761eab917e2c65556042d4": "/ced611daf7709cc778da928fec876475.eot",
    "af91c12f0f406a4f801aeb3b398768fe41d8f864": "/b7c9e1e479de3b53f1e4e30ebac2403a.woff",
    "ea960faa44ea26fc1c450f93c02b8107ec174ae5": "/748ce0709962c59bcf5beefc046b50e8.svg",
    "6c187ffdb822da7bf85f4eb2cbaa1e566de812a7": "/favicon.ico",
    "4d7e49e6082ab87c02d68f531d35393a50680a6c": "/79281281dbbe03395e8cee5fd218abdf.png",
    "3331eebdd4ba348ef25abe00c39ffbe867d46575": "/d41f55a78e6f49a5512878df1737e58a.ttf",
    "26fb8cecb5512223277b4d290a24492a0f09ede1": "/e79bfd88537def476913f3ed52f4f4b3.eot",
    "e9409adaf8b486500bb30bf02bd0fcffbe54b6bc": "/0cf70eea9b00fe0c198cece7efa250f0.jpg",
    "6d19e483d6eb507e1bb343e1f2ad83c2c0347b88": "/37a0b1b4182c6d7a4393aa6f2db203ce.png",
    "9d851d1b74981e57fa6f02bfc6b7e57ef22d9152": "/10e22ff7425634179e2bd73120edd5fc.png",
    "09963592e8c953cc7e14e3fb0a5b05d5042e8435": "/570eb83859dc23dd0eec423a49e147fe.woff2",
    "e652d3794576006df600235fa71e5b82d55e7d5b": "/b01d0a62ec3aca1ee2e08573243b1901.png",
    "623a7d0d26154898a38d8ab2b7d2cf04bed7ae69": "/ae88db1c637533b5930cab1330fda7be.png",
    "c6c953c2ccb2ca9abb21db8dbf473b5a435f0082": "/012cf6a10129e2275d79d6adac7f3b02.woff",
    "31eabf86b9ab86b1ac6beb2cc62be81ed42c0c60": "/d56c1f5aa44dffac58ca036110f7fd74.png",
    "8ee9e8954b4b3a2469c97b3119e56310e690c862": "/b03fa95f72a5e59a26501b47f217a81a.png",
    "fc05de31234e0090f7ddc28ce1b23af4026cb1da": "/a37b0c01c0baf1888ca812cc0508f6e2.ttf",
    "3143368b9f95b9e47fd40ea64cd5f7959b599a3e": "/965208574dd7682941b01182a37fecbd.png",
    "94dde4742b0d5e29f348355f20199435716055a2": "/a1f06b2c29dd94a18894849642a1ff48.png",
    "492d82c516e9a3ba9560bec76ce3f77b58c3ccb3": "/18bc17990313373e021bc00a1ccbd2db.jpg",
    "2f65861278ece3106f8823060584670c73757c4d": "/vendor.5f3f8e5c86cd2ed11594.chunk.js",
    "3802387e0cbcf831feddca80c310b1222468467f": "/1.e0fec2262e76af1349eb.chunk.js",
    "74fdf8551bf6fa1680249af98e28ef46c7f91a75": "/2.dcac4c350b44db2dfc9c.chunk.js",
    "0b0c24432a73924cdf2ac6a58b2fcfe532bfd27d": "/3.dcf741daca837765df13.chunk.js",
    "21f00e11cb6660607d6d23f4dd1c25669de4a12e": "/4.0c5cea92add1b543b226.chunk.js",
    "7c503dc66042c69802f884977ac1fb4a781dc793": "/main.16b18bf45ae123060fc5.chunk.js",
    "58b928ff81b85613efe70aabd416d075678db443": "/runtime~main.de03749dd05db6888013.js",
    "4ebace36d202eed9b875c6442d557e8c46c31d99": "/7.2e0c0be9ef7f4919726a.chunk.js",
    "251dccb313f14fea9767579cf6c28fcdffece9f3": "/8.af98932aaa1c28b61110.chunk.js",
    "8e5888f840488daadc4063f516559cf2071fbb50": "/9.14c101b26865ccde6156.chunk.js",
    "0a850bb9e541f47f0b8ac619364598e7c40b0ef1": "/10.cdce1c22bcc19009effe.chunk.js",
    "578e6867fe68ae82414fbea3d8a5df03a77b054b": "/11.c0dc4af915b38f8b0a97.chunk.js",
    "66c29a81601367acb85aed2a3f4cfec8d34b18d6": "/12.c0bb5e23360892a2df98.chunk.js",
    "c086a27ed19235726fdc43957231c83b53a1442d": "/13.7cffcce93b9bced53d05.chunk.js",
    "48390b8f44f9a91532cc15445fe7874010a8c2c5": "/14.986f39ef6cdbea251e19.chunk.js",
    "788ac84f1fee79c2d42c76c88cd15ed04d648a53": "/15.b85af442f4b72f5a1ee9.chunk.js",
    "eab064dff4480a173a820e25f857fd79cfc4dfb0": "/16.e5213846c941c2ee0e19.chunk.js",
    "18f3c93167951abec532f46a3a081792487221bf": "/17.0263b7abe7ad0ef75ffa.chunk.js",
    "a23fb9972477106be5ea0d5b6c2a259d083aaebf": "/18.b60d665e4058ff49ac91.chunk.js",
    "5a9d5b0932d05efd27384e7329243bcde3d295e7": "/19.c3c4c95d2ab80b8ac642.chunk.js",
    "c5c98a94edb6b834db626b380919e207aebe2f8a": "/20.324a83cb7b1e0037da23.chunk.js",
    "a4b5494a5ace8a8941abd553d9134c3c5a2b8211": "/21.d3a8f17999cd9fcfade1.chunk.js",
    "90cef833ae61ff90c2298a69e848bec803a825f4": "/22.9891379ebcb965747143.chunk.js",
    "d99473014f6b13f49193f6b43020639075e6f97b": "/23.4e067b74f93ad91da545.chunk.js",
    "9331496f25f3e4db64b9d67104f00e510f410d00": "/24.e4e59087b3b62cb37e02.chunk.js",
    "1354ea8ecb3e0a93af53774577eceddbf8fb3291": "/25.fb1d8fb98142a6e5d128.chunk.js",
    "4b0286a17842c13fe05d58220e6bca47c8a83b61": "/26.41e650a10520fa74b4ce.chunk.js",
    "49126fa00dc39a613c546fd68bd2ec5782a645dd": "/27.8e163394f68ba2fbf245.chunk.js",
    "b1daf7061064eaf6b413eedda6cc3d88b37db1e6": "/28.fb5233331e6210219469.chunk.js",
    "478aaf9354c312da5f792e5c9b104546a0d97e9a": "/29.cb2c83502a5fdd155d21.chunk.js",
    "462c2de87ab9436c509237aa1b9923deb7b4e6f1": "/30.e96e5d83a29b399e9e65.chunk.js",
    "72598f3bdb88704edb5aa2bb26d2d62f071d785b": "/31.f57dbaa62fc06630a355.chunk.js",
    "a682f914c5b58f35fc24b36a0f0699b0aaf6f51e": "/32.9077c5c8822fed31ab9e.chunk.js",
    "564bcc21e87d0e4438ebaf72d16834e492fc854a": "/33.42ddf9319fb79ae2f9e2.chunk.js",
    "7a3ca7d0970f2c802dd91297d20682920e5a41a0": "/34.6ecae660195d6e68ac68.chunk.js",
    "3b723fe707423613f55ea060e4424daf81b03b9d": "/35.4e09785ae9af1e5ddfbc.chunk.js",
    "1dbca3de5dbe7df141b6a214ccf0e4ba939e5cd3": "/36.267aa19469af75e5de9e.chunk.js",
    "27d8f495e2c79828c41b6d00363bdd6beff8fd58": "/37.04d517a04de8c240e326.chunk.js",
    "a34c6beba4e20299b3aad9e5a6854f0579031c3f": "/38.fb649a802484fd9deab9.chunk.js",
    "5aa984bc7abc58f88228fa8151e189b2e842fed0": "/39.1801bdd15594e2300ed2.chunk.js",
    "24c33ceb841a56fd4c3824f41c37a9592c8604e3": "/40.d233b1df05ba58716558.chunk.js",
    "8d732ed39fbd9949f1b8fe99367a2d5e1bc3d641": "/41.71cced34a66f39211c2c.chunk.js",
    "4dca44c9e1961b145a02a967b0e6d5ae2bf9c9c6": "/42.b5ac23e7de7ff9da849d.chunk.js",
    "321ef3229667d6c7dfabca013f42c8e1008f6cfc": "/43.aa5b678da96a9b7f3048.chunk.js",
    "d15b5f78b0c4130554e132c9e1ea657f34b37efe": "/"
  },
  "strategy": "changed",
  "responseStrategy": "cache-first",
  "version": "5/5/2021 09.25.59",
  "name": "webpack-offline",
  "pluginVersion": "5.0.6",
  "relativePaths": false
};

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "22249e1ea7baa06e7c1b");
/******/ })
/************************************************************************/
/******/ ({

/***/ "22249e1ea7baa06e7c1b":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {
  var waitUntil = ExtendableEvent.prototype.waitUntil;
  var respondWith = FetchEvent.prototype.respondWith;
  var promisesMap = new WeakMap();

  ExtendableEvent.prototype.waitUntil = function (promise) {
    var extendableEvent = this;
    var promises = promisesMap.get(extendableEvent);

    if (promises) {
      promises.push(Promise.resolve(promise));
      return;
    }

    promises = [Promise.resolve(promise)];
    promisesMap.set(extendableEvent, promises);

    // call original method
    return waitUntil.call(extendableEvent, Promise.resolve().then(function processPromises() {
      var len = promises.length;

      // wait for all to settle
      return Promise.all(promises.map(function (p) {
        return p["catch"](function () {});
      })).then(function () {
        // have new items been added? If so, wait again
        if (promises.length != len) return processPromises();
        // we're done!
        promisesMap["delete"](extendableEvent);
        // reject if one of the promises rejected
        return Promise.all(promises);
      });
    }));
  };

  FetchEvent.prototype.respondWith = function (promise) {
    this.waitUntil(promise);
    return respondWith.call(this, promise);
  };
})();;
        'use strict';

if (typeof DEBUG === 'undefined') {
  var DEBUG = false;
}

function WebpackServiceWorker(params, helpers) {
  var cacheMaps = helpers.cacheMaps;
  // navigationPreload: true, { map: (URL) => URL, test: (URL) => boolean }
  var navigationPreload = helpers.navigationPreload;

  // (update)strategy: changed, all
  var strategy = params.strategy;
  // responseStrategy: cache-first, network-first
  var responseStrategy = params.responseStrategy;

  var assets = params.assets;

  var hashesMap = params.hashesMap;
  var externals = params.externals;

  var prefetchRequest = params.prefetchRequest || {
    credentials: 'same-origin',
    mode: 'cors'
  };

  var CACHE_PREFIX = params.name;
  var CACHE_TAG = params.version;
  var CACHE_NAME = CACHE_PREFIX + ':' + CACHE_TAG;

  var PRELOAD_CACHE_NAME = CACHE_PREFIX + '$preload';
  var STORED_DATA_KEY = '__offline_webpack__data';

  mapAssets();

  var allAssets = [].concat(assets.main, assets.additional, assets.optional);

  self.addEventListener('install', function (event) {
    console.log('[SW]:', 'Install event');

    var installing = undefined;

    if (strategy === 'changed') {
      installing = cacheChanged('main');
    } else {
      installing = cacheAssets('main');
    }

    event.waitUntil(installing);
  });

  self.addEventListener('activate', function (event) {
    console.log('[SW]:', 'Activate event');

    var activation = cacheAdditional();

    // Delete all assets which name starts with CACHE_PREFIX and
    // is not current cache (CACHE_NAME)
    activation = activation.then(storeCacheData);
    activation = activation.then(deleteObsolete);
    activation = activation.then(function () {
      if (self.clients && self.clients.claim) {
        return self.clients.claim();
      }
    });

    if (navigationPreload && self.registration.navigationPreload) {
      activation = Promise.all([activation, self.registration.navigationPreload.enable()]);
    }

    event.waitUntil(activation);
  });

  function cacheAdditional() {
    if (!assets.additional.length) {
      return Promise.resolve();
    }

    if (DEBUG) {
      console.log('[SW]:', 'Caching additional');
    }

    var operation = undefined;

    if (strategy === 'changed') {
      operation = cacheChanged('additional');
    } else {
      operation = cacheAssets('additional');
    }

    // Ignore fail of `additional` cache section
    return operation['catch'](function (e) {
      console.error('[SW]:', 'Cache section `additional` failed to load');
    });
  }

  function cacheAssets(section) {
    var batch = assets[section];

    return caches.open(CACHE_NAME).then(function (cache) {
      return addAllNormalized(cache, batch, {
        bust: params.version,
        request: prefetchRequest,
        failAll: section === 'main'
      });
    }).then(function () {
      logGroup('Cached assets: ' + section, batch);
    })['catch'](function (e) {
      console.error(e);
      throw e;
    });
  }

  function cacheChanged(section) {
    return getLastCache().then(function (args) {
      if (!args) {
        return cacheAssets(section);
      }

      var lastCache = args[0];
      var lastKeys = args[1];
      var lastData = args[2];

      var lastMap = lastData.hashmap;
      var lastVersion = lastData.version;

      if (!lastData.hashmap || lastVersion === params.version) {
        return cacheAssets(section);
      }

      var lastHashedAssets = Object.keys(lastMap).map(function (hash) {
        return lastMap[hash];
      });

      var lastUrls = lastKeys.map(function (req) {
        var url = new URL(req.url);
        url.search = '';
        url.hash = '';

        return url.toString();
      });

      var sectionAssets = assets[section];
      var moved = [];
      var changed = sectionAssets.filter(function (url) {
        if (lastUrls.indexOf(url) === -1 || lastHashedAssets.indexOf(url) === -1) {
          return true;
        }

        return false;
      });

      Object.keys(hashesMap).forEach(function (hash) {
        var asset = hashesMap[hash];

        // Return if not in sectionAssets or in changed or moved array
        if (sectionAssets.indexOf(asset) === -1 || changed.indexOf(asset) !== -1 || moved.indexOf(asset) !== -1) return;

        var lastAsset = lastMap[hash];

        if (lastAsset && lastUrls.indexOf(lastAsset) !== -1) {
          moved.push([lastAsset, asset]);
        } else {
          changed.push(asset);
        }
      });

      logGroup('Changed assets: ' + section, changed);
      logGroup('Moved assets: ' + section, moved);

      var movedResponses = Promise.all(moved.map(function (pair) {
        return lastCache.match(pair[0]).then(function (response) {
          return [pair[1], response];
        });
      }));

      return caches.open(CACHE_NAME).then(function (cache) {
        var move = movedResponses.then(function (responses) {
          return Promise.all(responses.map(function (pair) {
            return cache.put(pair[0], pair[1]);
          }));
        });

        return Promise.all([move, addAllNormalized(cache, changed, {
          bust: params.version,
          request: prefetchRequest,
          failAll: section === 'main',
          deleteFirst: section !== 'main'
        })]);
      });
    });
  }

  function deleteObsolete() {
    return caches.keys().then(function (keys) {
      var all = keys.map(function (key) {
        if (key.indexOf(CACHE_PREFIX) !== 0 || key.indexOf(CACHE_NAME) === 0) return;

        console.log('[SW]:', 'Delete cache:', key);
        return caches['delete'](key);
      });

      return Promise.all(all);
    });
  }

  function getLastCache() {
    return caches.keys().then(function (keys) {
      var index = keys.length;
      var key = undefined;

      while (index--) {
        key = keys[index];

        if (key.indexOf(CACHE_PREFIX) === 0) {
          break;
        }
      }

      if (!key) return;

      var cache = undefined;

      return caches.open(key).then(function (_cache) {
        cache = _cache;
        return _cache.match(new URL(STORED_DATA_KEY, location).toString());
      }).then(function (response) {
        if (!response) return;

        return Promise.all([cache, cache.keys(), response.json()]);
      });
    });
  }

  function storeCacheData() {
    return caches.open(CACHE_NAME).then(function (cache) {
      var data = new Response(JSON.stringify({
        version: params.version,
        hashmap: hashesMap
      }));

      return cache.put(new URL(STORED_DATA_KEY, location).toString(), data);
    });
  }

  self.addEventListener('fetch', function (event) {
    // Handle only GET requests
    if (event.request.method !== 'GET') {
      return;
    }

    // This prevents some weird issue with Chrome DevTools and 'only-if-cached'
    // Fixes issue #385, also ref to:
    // - https://github.com/paulirish/caltrainschedule.io/issues/49
    // - https://bugs.chromium.org/p/chromium/issues/detail?id=823392
    if (event.request.cache === 'only-if-cached' && event.request.mode !== 'same-origin') {
      return;
    }

    var url = new URL(event.request.url);
    url.hash = '';

    var urlString = url.toString();

    // Not external, so search part of the URL should be stripped,
    // if it's external URL, the search part should be kept
    if (externals.indexOf(urlString) === -1) {
      url.search = '';
      urlString = url.toString();
    }

    var assetMatches = allAssets.indexOf(urlString) !== -1;
    var cacheUrl = urlString;

    if (!assetMatches) {
      var cacheRewrite = matchCacheMap(event.request);

      if (cacheRewrite) {
        cacheUrl = cacheRewrite;
        assetMatches = true;
      }
    }

    if (!assetMatches) {
      // Use request.mode === 'navigate' instead of isNavigateRequest
      // because everything what supports navigationPreload supports
      // 'navigate' request.mode
      if (event.request.mode === 'navigate') {
        // Requesting with fetchWithPreload().
        // Preload is used only if navigationPreload is enabled and
        // navigationPreload mapping is not used.
        if (navigationPreload === true) {
          event.respondWith(fetchWithPreload(event));
          return;
        }
      }

      // Something else, positive, but not `true`
      if (navigationPreload) {
        var preloadedResponse = retrivePreloadedResponse(event);

        if (preloadedResponse) {
          event.respondWith(preloadedResponse);
          return;
        }
      }

      // Logic exists here if no cache match
      return;
    }

    // Cache handling/storing/fetching starts here
    var resource = undefined;

    if (responseStrategy === 'network-first') {
      resource = networkFirstResponse(event, urlString, cacheUrl);
    }
    // 'cache-first' otherwise
    // (responseStrategy has been validated before)
    else {
        resource = cacheFirstResponse(event, urlString, cacheUrl);
      }

    event.respondWith(resource);
  });

  self.addEventListener('message', function (e) {
    var data = e.data;
    if (!data) return;

    switch (data.action) {
      case 'skipWaiting':
        {
          if (self.skipWaiting) self.skipWaiting();
        }break;
    }
  });

  function cacheFirstResponse(event, urlString, cacheUrl) {
    handleNavigationPreload(event);

    return cachesMatch(cacheUrl, CACHE_NAME).then(function (response) {
      if (response) {
        if (DEBUG) {
          console.log('[SW]:', 'URL [' + cacheUrl + '](' + urlString + ') from cache');
        }

        return response;
      }

      // Load and cache known assets
      var fetching = fetch(event.request).then(function (response) {
        if (!response.ok) {
          if (DEBUG) {
            console.log('[SW]:', 'URL [' + urlString + '] wrong response: [' + response.status + '] ' + response.type);
          }

          return response;
        }

        if (DEBUG) {
          console.log('[SW]:', 'URL [' + urlString + '] from network');
        }

        if (cacheUrl === urlString) {
          (function () {
            var responseClone = response.clone();
            var storing = caches.open(CACHE_NAME).then(function (cache) {
              return cache.put(urlString, responseClone);
            }).then(function () {
              console.log('[SW]:', 'Cache asset: ' + urlString);
            });

            event.waitUntil(storing);
          })();
        }

        return response;
      });

      return fetching;
    });
  }

  function networkFirstResponse(event, urlString, cacheUrl) {
    return fetchWithPreload(event).then(function (response) {
      if (response.ok) {
        if (DEBUG) {
          console.log('[SW]:', 'URL [' + urlString + '] from network');
        }

        return response;
      }

      // Throw to reach the code in the catch below
      throw response;
    })
    // This needs to be in a catch() and not just in the then() above
    // cause if your network is down, the fetch() will throw
    ['catch'](function (erroredResponse) {
      if (DEBUG) {
        console.log('[SW]:', 'URL [' + urlString + '] from cache if possible');
      }

      return cachesMatch(cacheUrl, CACHE_NAME).then(function (response) {
        if (response) {
          return response;
        }

        if (erroredResponse instanceof Response) {
          return erroredResponse;
        }

        // Not a response at this point, some other error
        throw erroredResponse;
        // return Response.error();
      });
    });
  }

  function handleNavigationPreload(event) {
    if (navigationPreload && typeof navigationPreload.map === 'function' &&
    // Use request.mode === 'navigate' instead of isNavigateRequest
    // because everything what supports navigationPreload supports
    // 'navigate' request.mode
    event.preloadResponse && event.request.mode === 'navigate') {
      var mapped = navigationPreload.map(new URL(event.request.url), event.request);

      if (mapped) {
        storePreloadedResponse(mapped, event);
      }
    }
  }

  // Temporary in-memory store for faster access
  var navigationPreloadStore = new Map();

  function storePreloadedResponse(_url, event) {
    var url = new URL(_url, location);
    var preloadResponsePromise = event.preloadResponse;

    navigationPreloadStore.set(preloadResponsePromise, {
      url: url,
      response: preloadResponsePromise
    });

    var isSamePreload = function isSamePreload() {
      return navigationPreloadStore.has(preloadResponsePromise);
    };

    var storing = preloadResponsePromise.then(function (res) {
      // Return if preload isn't enabled or hasn't happened
      if (!res) return;

      // If navigationPreloadStore already consumed
      // or navigationPreloadStore already contains another preload,
      // then do not store anything and return
      if (!isSamePreload()) {
        return;
      }

      var clone = res.clone();

      // Storing the preload response for later consume (hasn't yet been consumed)
      return caches.open(PRELOAD_CACHE_NAME).then(function (cache) {
        if (!isSamePreload()) return;

        return cache.put(url, clone).then(function () {
          if (!isSamePreload()) {
            return caches.open(PRELOAD_CACHE_NAME).then(function (cache) {
              return cache['delete'](url);
            });
          }
        });
      });
    });

    event.waitUntil(storing);
  }

  function retriveInMemoryPreloadedResponse(url) {
    if (!navigationPreloadStore) {
      return;
    }

    var foundResponse = undefined;
    var foundKey = undefined;

    navigationPreloadStore.forEach(function (store, key) {
      if (store.url.href === url.href) {
        foundResponse = store.response;
        foundKey = key;
      }
    });

    if (foundResponse) {
      navigationPreloadStore['delete'](foundKey);
      return foundResponse;
    }
  }

  function retrivePreloadedResponse(event) {
    var url = new URL(event.request.url);

    if (self.registration.navigationPreload && navigationPreload && navigationPreload.test && navigationPreload.test(url, event.request)) {} else {
      return;
    }

    var fromMemory = retriveInMemoryPreloadedResponse(url);
    var request = event.request;

    if (fromMemory) {
      event.waitUntil(caches.open(PRELOAD_CACHE_NAME).then(function (cache) {
        return cache['delete'](request);
      }));

      return fromMemory;
    }

    return cachesMatch(request, PRELOAD_CACHE_NAME).then(function (response) {
      if (response) {
        event.waitUntil(caches.open(PRELOAD_CACHE_NAME).then(function (cache) {
          return cache['delete'](request);
        }));
      }

      return response || fetch(event.request);
    });
  }

  function mapAssets() {
    Object.keys(assets).forEach(function (key) {
      assets[key] = assets[key].map(function (path) {
        var url = new URL(path, location);

        url.hash = '';

        if (externals.indexOf(path) === -1) {
          url.search = '';
        }

        return url.toString();
      });
    });

    hashesMap = Object.keys(hashesMap).reduce(function (result, hash) {
      var url = new URL(hashesMap[hash], location);
      url.search = '';
      url.hash = '';

      result[hash] = url.toString();
      return result;
    }, {});

    externals = externals.map(function (path) {
      var url = new URL(path, location);
      url.hash = '';

      return url.toString();
    });
  }

  function addAllNormalized(cache, requests, options) {
    var bustValue = options.bust;
    var failAll = options.failAll !== false;
    var deleteFirst = options.deleteFirst === true;
    var requestInit = options.request || {
      credentials: 'omit',
      mode: 'cors'
    };

    var deleting = Promise.resolve();

    if (deleteFirst) {
      deleting = Promise.all(requests.map(function (request) {
        return cache['delete'](request)['catch'](function () {});
      }));
    }

    return Promise.all(requests.map(function (request) {
      if (bustValue) {
        request = applyCacheBust(request, bustValue);
      }

      return fetch(request, requestInit).then(fixRedirectedResponse).then(function (response) {
        if (!response.ok) {
          return { error: true };
        }

        return { response: response };
      }, function () {
        return { error: true };
      });
    })).then(function (responses) {
      if (failAll && responses.some(function (data) {
        return data.error;
      })) {
        return Promise.reject(new Error('Wrong response status'));
      }

      if (!failAll) {
        responses = responses.filter(function (data) {
          return !data.error;
        });
      }

      return deleting.then(function () {
        var addAll = responses.map(function (_ref, i) {
          var response = _ref.response;

          return cache.put(requests[i], response);
        });

        return Promise.all(addAll);
      });
    });
  }

  function matchCacheMap(request) {
    var urlString = request.url;
    var url = new URL(urlString);

    var requestType = undefined;

    if (isNavigateRequest(request)) {
      requestType = 'navigate';
    } else if (url.origin === location.origin) {
      requestType = 'same-origin';
    } else {
      requestType = 'cross-origin';
    }

    for (var i = 0; i < cacheMaps.length; i++) {
      var map = cacheMaps[i];

      if (!map) continue;
      if (map.requestTypes && map.requestTypes.indexOf(requestType) === -1) {
        continue;
      }

      var newString = undefined;

      if (typeof map.match === 'function') {
        newString = map.match(url, request);
      } else {
        newString = urlString.replace(map.match, map.to);
      }

      if (newString && newString !== urlString) {
        return newString;
      }
    }
  }

  function fetchWithPreload(event) {
    if (!event.preloadResponse || navigationPreload !== true) {
      return fetch(event.request);
    }

    return event.preloadResponse.then(function (response) {
      return response || fetch(event.request);
    });
  }
}

function cachesMatch(request, cacheName) {
  return caches.match(request, {
    cacheName: cacheName
  }).then(function (response) {
    if (isNotRedirectedResponse(response)) {
      return response;
    }

    // Fix already cached redirected responses
    return fixRedirectedResponse(response).then(function (fixedResponse) {
      return caches.open(cacheName).then(function (cache) {
        return cache.put(request, fixedResponse);
      }).then(function () {
        return fixedResponse;
      });
    });
  })
  // Return void if error happened (cache not found)
  ['catch'](function () {});
}

function applyCacheBust(asset, key) {
  var hasQuery = asset.indexOf('?') !== -1;
  return asset + (hasQuery ? '&' : '?') + '__uncache=' + encodeURIComponent(key);
}

function isNavigateRequest(request) {
  return request.mode === 'navigate' || request.headers.get('Upgrade-Insecure-Requests') || (request.headers.get('Accept') || '').indexOf('text/html') !== -1;
}

function isNotRedirectedResponse(response) {
  return !response || !response.redirected || !response.ok || response.type === 'opaqueredirect';
}

// Based on https://github.com/GoogleChrome/sw-precache/pull/241/files#diff-3ee9060dc7a312c6a822cac63a8c630bR85
function fixRedirectedResponse(response) {
  if (isNotRedirectedResponse(response)) {
    return Promise.resolve(response);
  }

  var body = 'body' in response ? Promise.resolve(response.body) : response.blob();

  return body.then(function (data) {
    return new Response(data, {
      headers: response.headers,
      status: response.status
    });
  });
}

function copyObject(original) {
  return Object.keys(original).reduce(function (result, key) {
    result[key] = original[key];
    return result;
  }, {});
}

function logGroup(title, assets) {
  console.groupCollapsed('[SW]:', title);

  assets.forEach(function (asset) {
    console.log('Asset:', asset);
  });

  console.groupEnd();
}
        WebpackServiceWorker(__wpo, {
loaders: {},
cacheMaps: [
      {
      match: function(url) {
          if (url.pathname === location.pathname) {
            return;
          }

          return new URL("/", location);
        },
      to: null,
      requestTypes: ["navigate"],
    }
    ],
navigationPreload: false,
});
        module.exports = __webpack_require__("6872a71ed75a597694c7")
      

/***/ }),

/***/ "6872a71ed75a597694c7":
/***/ (function(module, exports) {



/***/ })

/******/ });